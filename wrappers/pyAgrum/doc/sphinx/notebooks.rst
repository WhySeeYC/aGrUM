Tutorials on pyAgrum
====================

.. nbgallery::
    :glob:

    notebooks/*-Tutorial*
    
Exact and Approxiomated Inference
=================================

.. nbgallery::
    :glob:

    notebooks/*-Inference*


Learning Bayesian networks
==========================

.. nbgallery::
    :glob:

    notebooks/*-Learning*

Different Graphical Models
==========================

.. nbgallery::
    :glob:

    notebooks/*-Models*

Bayesian networks as scikit-learn compliant classifiers
=======================================================

.. nbgallery::
    :glob:

    notebooks/*-Classifier*

Causal Bayesian Networks
========================

.. nbgallery::
    :glob:

    notebooks/*-Causality*

Examples
========

.. nbgallery::
    :glob:

    notebooks/*-Examples*
    notebooks/*-Applications*

pyAgrum's specific features
===========================

.. nbgallery::
    :glob:

    notebooks/*-Tools*
